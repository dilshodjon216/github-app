package com.dilshodjon216.githubapp.framgemnt

import android.app.ProgressDialog
import android.content.ClipData
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.dilshodjon216.githubapp.R
import com.dilshodjon216.githubapp.adapotor.SubscriptionAdaptor
import com.dilshodjon216.githubapp.model.Subscriptions
import com.dilshodjon216.githubapp.retrofit.ApiClient
import com.dilshodjon216.githubapp.retrofit.ApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SubscriptionsFragment : Fragment() {

    lateinit var progressDialog: ProgressDialog
    lateinit var sharedpreferences: SharedPreferences
    private var MyPREFERENCES: String = "githubApp"
    private lateinit var subscriptionsRV:RecyclerView
    lateinit var subscriptions:ArrayList<Subscriptions>
    lateinit var subscriptionAdaptor: SubscriptionAdaptor
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var view= inflater.inflate(R.layout.fragment_subscriptions, container, false)
        progressDialog= ProgressDialog(context)
        subscriptionsRV=view.findViewById(R.id.subscriptionsRV)
        sharedpreferences =
            context!!.getSharedPreferences(MyPREFERENCES, AppCompatActivity.MODE_PRIVATE)
        var userName = sharedpreferences.getString("userLogin", "")
        subscriptions= ArrayList()

        subscriptionAdaptor= SubscriptionAdaptor(subscriptions,object:SubscriptionAdaptor.OnClickText{
            override fun onClick(subscriptions: Subscriptions) {
                setClipboard(context!!, subscriptions.clone_url)
                Toast.makeText(context!!,"Ma'lumot saqladi",Toast.LENGTH_SHORT).show()
            }

        },context!!)
        subscriptionsRV.adapter=subscriptionAdaptor
        
        getSubscriptions(userName!!)

        
        return view
    }

    private fun setClipboard(context: Context, text: String) {
        val clipboard =
            context.getSystemService(Context.CLIPBOARD_SERVICE) as android.content.ClipboardManager
        val clip = ClipData.newPlainText("Copied Text", text)
        clipboard.setPrimaryClip(clip)
    }

    private fun getSubscriptions(userName: String) {

        progressDialog.setMessage("Data is loading...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        var apiClient=ApiClient.getRetrofit()
        var apiService=apiClient.create(ApiService::class.java)
        apiService.getSubcriptions(userName).enqueue(object : Callback<List<Subscriptions>> {
            override fun onResponse(
                call: Call<List<Subscriptions>>,
                response: Response<List<Subscriptions>>
            ) {
                if (response.isSuccessful) {
                    subscriptions.addAll(response.body()!!)
                    subscriptionAdaptor.notifyDataSetChanged()
                    progressDialog.dismiss()
                } else {
                    progressDialog.dismiss()
                    Toast.makeText(context, "Response ${response.code()}", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<List<Subscriptions>>, t: Throwable) {
                progressDialog.dismiss()
                Toast.makeText(context, "Failure internet", Toast.LENGTH_SHORT).show()
            }

        })

    }


}