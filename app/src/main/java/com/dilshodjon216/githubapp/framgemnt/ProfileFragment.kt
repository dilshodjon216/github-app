package com.dilshodjon216.githubapp.framgemnt

import android.app.ProgressDialog
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.dilshodjon216.githubapp.R
import com.dilshodjon216.githubapp.adapotor.UsersAdatoptor
import com.dilshodjon216.githubapp.model.Users
import com.dilshodjon216.githubapp.retrofit.ApiClient
import com.dilshodjon216.githubapp.retrofit.ApiService
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_dashbord.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ProfileFragment : Fragment() {

    lateinit var sharedpreferences: SharedPreferences
    private var MyPREFERENCES: String = "githubApp"
    private lateinit var imageView: ImageView
    private lateinit var usernameValue1: TextView
    private lateinit var userloginValue1: TextView
    private lateinit var usercompanyValueTV1: TextView
    private lateinit var useremailValueTV1: TextView
    private lateinit var userloctionvalueTV1: TextView

    private lateinit var progressDialog: ProgressDialog

    @Suppress("DEPRECATION")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var view = inflater.inflate(R.layout.fragment_profile, container, false)
        sharedpreferences =
            context!!.getSharedPreferences(MyPREFERENCES, AppCompatActivity.MODE_PRIVATE)
        var userName = sharedpreferences.getString("userLogin", "")
        var installApp = sharedpreferences.getString("install", "")

        progressDialog = ProgressDialog(context)
        imageView = view.findViewById(R.id.userprofilimage1)
        usernameValue1 = view.findViewById(R.id.usernameValue1)
        userloginValue1 = view.findViewById(R.id.userloginValue1)
        usercompanyValueTV1 = view.findViewById(R.id.usercompanyValueTV1)
        useremailValueTV1 = view.findViewById(R.id.useremailValueTV1)
        userloctionvalueTV1 = view.findViewById(R.id.userloctionvalueTV1)


        if (installApp.equals("true")) {
            sharedRead()
        } else {
            getProfile(userName)
        }




        return view
    }

    private fun sharedRead() {
        var userlogin = sharedpreferences.getString("userName", "")
        var userName = sharedpreferences.getString("name", "")
        var userImage = sharedpreferences.getString("userImage", "")
        var userComp = sharedpreferences.getString("company", "")
        var email = sharedpreferences.getString("email", "")
        var userLocation = sharedpreferences.getString("location", "")
        usernameValue1.text = userName
        userloginValue1.text = userlogin
        usercompanyValueTV1.text = userComp
        useremailValueTV1.text = email
        userloctionvalueTV1.text = userLocation
        Picasso.get().load(userImage).placeholder(R.drawable.ic_baseline_account_circle_24).into(
            imageView
        )
    }

    private fun getProfile(userName: String?) {

        progressDialog.setMessage("Data is loading...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        var apiClient = ApiClient.getRetrofit()
        var apiService = apiClient.create(ApiService::class.java)

        apiService.getUser(userName!!).enqueue(object : Callback<Users> {
            override fun onResponse(call: Call<Users>, response: Response<Users>) {
                if (response.isSuccessful) {
                    var user = response.body()

                    usernameValue1.text = user?.name
                    userloginValue1.text = user?.login
                    usercompanyValueTV1.text = user?.company
                    useremailValueTV1.text = user?.email
                    userloctionvalueTV1.text = user?.location

                    val editor = sharedpreferences.edit()
                    editor.putString("company", user?.company)
                    editor.putString("email", user?.email)
                    editor.putString("location", user?.location)
                    editor.putString("name", user?.name)
                    editor.putString("install","true")
                    editor.apply()

                    Picasso.get().load(user?.avatar_url)
                        .placeholder(R.drawable.ic_baseline_account_circle_24).into(
                            imageView
                        )
                    progressDialog.dismiss()

                } else {

                    Toast.makeText(context,"Response ${response.code()}",Toast.LENGTH_SHORT).show()
                    progressDialog.dismiss()
                }
            }

            override fun onFailure(call: Call<Users>, t: Throwable) {
                Toast.makeText(context,"Internt bilan mu'amo mavjud",Toast.LENGTH_SHORT).show()
                progressDialog.dismiss()
            }

        })
    }


}