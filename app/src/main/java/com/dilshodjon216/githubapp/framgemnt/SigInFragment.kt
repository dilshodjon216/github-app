package com.dilshodjon216.githubapp.framgemnt

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.dilshodjon216.githubapp.R
import com.dilshodjon216.githubapp.activity.DashbordActivity
import com.dilshodjon216.githubapp.model.Users
import com.dilshodjon216.githubapp.retrofit.ApiClient
import com.dilshodjon216.githubapp.retrofit.ApiService
import kotlinx.android.synthetic.main.fragment_sig_in.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SigInFragment : Fragment() {

    lateinit var progressDialog: ProgressDialog
    lateinit var sharedpreferences: SharedPreferences
   lateinit var  usenameEt: EditText
    private var MyPREFERENCES: String = "githubApp"





    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var root = inflater.inflate(R.layout.fragment_sig_in, container, false)
        progressDialog = ProgressDialog(context)
        sharedpreferences =
            context!!.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE)
        usenameEt=root.usernameET

        root.signInbtn.setOnClickListener {



             var login=usenameEt.text.toString()

            if (login != "") {
                progressDialog.setMessage("The information is being verified")
                progressDialog.setCancelable(false)
                progressDialog.show()
                findUserBylogin(login)

            } else {
                Toast.makeText(context, "Username not entered", Toast.LENGTH_SHORT)
            }

        }

        root.signUpBtn.setOnClickListener {
            val url =
                "https://github.com/join?ref_cta=Sign+up&ref_loc=header+logged+out&ref_page=%2F&source=header-home"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }
        return root
    }

    private fun findUserBylogin(login: String) {

        var apiclient = ApiClient.getRetrofit()
        var apiSerice = apiclient.create(ApiService::class.java)

        apiSerice.getUser(login).enqueue(object :Callback<Users>{
            override fun onResponse(call: Call<Users>, response: Response<Users>) {
               if(response.isSuccessful){
                   println(response.code())
                   if (response.code()==200){

                       val editor = sharedpreferences.edit()
                       editor.putString("userLogin", response.body()?.login)
                       editor.putString("userImage",response.body()?.avatar_url)
                       editor.apply()

                       val intent = Intent(activity, DashbordActivity::class.java)
                       activity!!.startActivity(intent)
                       activity!!.finish()
                        progressDialog.dismiss()
                       Toast.makeText(context, "boshladik", Toast.LENGTH_SHORT)
                   }else{
                       Toast.makeText(context, "bunday user yo'q", Toast.LENGTH_SHORT)
                       progressDialog.dismiss()
                   }
               }else{
                   Toast.makeText(context, "Intnernt bilan mumao bor2", Toast.LENGTH_SHORT)
                   progressDialog.dismiss()
               }
            }

            override fun onFailure(call: Call<Users>, t: Throwable) {
                Toast.makeText(context, "Intnernt bilan mumao bor1", Toast.LENGTH_SHORT)
                progressDialog.dismiss()
            }

        })


    }



}