package com.dilshodjon216.githubapp.framgemnt

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dilshodjon216.githubapp.R
import com.dilshodjon216.githubapp.model.Users
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_dashbord.*
import kotlinx.android.synthetic.main.fragment_user_profile.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
  lateinit var users:Users


class UserProfileFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    lateinit var users: Users
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view= inflater.inflate(R.layout.fragment_user_profile, container, false)

        val arguments=arguments
         val username=arguments?.getString("userName")
        val userImage=arguments?.getString("userImage")
        val userLogin=arguments?.getString("userLogin")
        val userComany=arguments?.getString("userComany")
        val userLociton=arguments?.getString("userLociton")
        val userEmail=arguments?.getString("userEmail")

        Picasso.get().load(userImage).placeholder(R.drawable.ic_baseline_account_circle_24).into(
            view.userprofilimage
        )
        view.usernameValue.text=username
        view.userloginValue.text=userLogin
        view.usercompanyValueTV.text=userComany
        view.useremailValueTV.text=userEmail
        view.userloctionvalueTV.text=userLociton


        return view
    }


}