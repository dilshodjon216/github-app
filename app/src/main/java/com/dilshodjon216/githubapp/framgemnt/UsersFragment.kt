package com.dilshodjon216.githubapp.framgemnt

import android.app.ProgressDialog
import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.dilshodjon216.githubapp.R
import com.dilshodjon216.githubapp.adapotor.UsersAdatoptor
import com.dilshodjon216.githubapp.model.Users
import com.dilshodjon216.githubapp.retrofit.ApiClient
import com.dilshodjon216.githubapp.retrofit.ApiService
import kotlinx.android.synthetic.main.fragment_users.*
import kotlinx.android.synthetic.main.fragment_users.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class UsersFragment : Fragment() {

    lateinit var progressDialog: ProgressDialog
    lateinit var usersLoginList:ArrayList<Users>
    lateinit var usersList:ArrayList<Users>
   lateinit var usersAdatoptor: UsersAdatoptor
    private lateinit var usersRV:RecyclerView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
       var view=inflater.inflate(R.layout.fragment_users, container, false)
        progressDialog= ProgressDialog(context)
        usersList= ArrayList()

         usersAdatoptor= UsersAdatoptor(usersList,object :UsersAdatoptor.OnItemClick{
             override fun onClick(users: Users) {


                 val registerFragment = UserProfileFragment()
                 val mArgs = Bundle()
                 mArgs.putString("userImage",users.avatar_url)
                 mArgs.putString("userName",users.name)
                 mArgs.putString("userLogin",users.login)
                 mArgs.putString("userComany",users.company)
                 mArgs.putString("userLociton",users.location)
                 mArgs.putString("userEmail",users.email)


                 registerFragment.arguments=mArgs
                 fragmentManager?.beginTransaction()?.replace(R.id.contentII, registerFragment)
                     ?.setCustomAnimations(R.anim.enter_left_to_right,R.anim.exit_right_to_left)
                     ?.addToBackStack(registerFragment.toString())
                     ?.commit()
             }

         })
        usersRV=view.findViewById(R.id.usersRV)

        usersRV.adapter=usersAdatoptor

        getUsers()

        return view
    }

    @Suppress("DEPRECATION")
    private fun getUsers() {
       progressDialog.setMessage("Data is loading...")
       progressDialog.setCancelable(false)
        progressDialog.show()
        usersLoginList= ArrayList()

        var apiClient=ApiClient.getRetrofit()
        var apiSer=apiClient.create(ApiService::class.java)



        apiSer.getUsers().enqueue(object:Callback<List<Users>>{
            override fun onResponse(call: Call<List<Users>>, response: Response<List<Users>>) {
                if(response.isSuccessful){
                    usersLoginList.addAll(response.body()!!)
                    Log.w("kelidm->",usersLoginList.toString())
                    for(user:Users in usersLoginList){
                        println(user.toString())
                        getUser(user.login)
                    }

                }else{
                    progressDialog.dismiss()
                    Toast.makeText(context,"Response ${response.code()}",Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<List<Users>>, t: Throwable) {
                progressDialog.dismiss()
                Toast.makeText(context,"Unable to connect to the Internet ",Toast.LENGTH_SHORT).show()
            }

        })



    }

    private fun getUser(login: String) {
        var apiClient=ApiClient.getRetrofit()
        var apiSer=apiClient.create(ApiService::class.java)
        apiSer.getUser(login).enqueue(object : Callback<Users>{
            override fun onResponse(call: Call<Users>, response: Response<Users>) {
                if(response.isSuccessful){
                    usersList.add(response.body()!!)
                    println(usersList.toString())
                    usersAdatoptor.notifyItemInserted(usersList.size)
                    progressDialog.dismiss()
                }else
                {
                    progressDialog.dismiss()
                    Toast.makeText(context,"Response ${response.code()}",Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<Users>, t: Throwable) {
                progressDialog.dismiss()
                Toast.makeText(context,"Unable to connect to the Internet ",Toast.LENGTH_SHORT).show()
            }

        })
    }

}