package com.dilshodjon216.githubapp.framgemnt

import android.app.ProgressDialog
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.dilshodjon216.githubapp.R
import com.dilshodjon216.githubapp.adapotor.UsersAdatoptor
import com.dilshodjon216.githubapp.model.Users
import com.dilshodjon216.githubapp.retrofit.ApiClient
import com.dilshodjon216.githubapp.retrofit.ApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class FollowersFragment : Fragment() {

    lateinit var progressDialog: ProgressDialog
    lateinit var sharedpreferences: SharedPreferences
    private var MyPREFERENCES: String = "githubApp"
    lateinit var usersList: ArrayList<Users>
    lateinit var usersAdatoptor: UsersAdatoptor
    private lateinit var followersRV: RecyclerView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_followers, container, false)
        progressDialog = ProgressDialog(context)

        sharedpreferences =
            context!!.getSharedPreferences(MyPREFERENCES, AppCompatActivity.MODE_PRIVATE)
        var userName = sharedpreferences.getString("userLogin", "")
        usersList = ArrayList()

        usersAdatoptor = UsersAdatoptor(usersList, object : UsersAdatoptor.OnItemClick {
            override fun onClick(users: Users) {


                val registerFragment = UserProfileFragment()
                val mArgs = Bundle()
                mArgs.putString("userImage",users.avatar_url)
                mArgs.putString("userName",users.name)
                mArgs.putString("userLogin",users.login)
                mArgs.putString("userComany",users.company)
                mArgs.putString("userLociton",users.location)
                mArgs.putString("userEmail",users.email)

                fragmentManager?.beginTransaction()?.replace(R.id.contentII, registerFragment)
                    ?.setCustomAnimations(R.anim.enter_left_to_right, R.anim.exit_right_to_left)
                    ?.addToBackStack(registerFragment.toString())
                    ?.commit()
            }

        })

        followersRV = view.findViewById(R.id.followersRV)

        followersRV.adapter = usersAdatoptor

        getFollowers(userName)

        return view
    }

    private fun getFollowers(userName: String?) {
        progressDialog.setMessage("Data is loading...")
        progressDialog.setCancelable(false)
        progressDialog.show()
        var apiClient = ApiClient.getRetrofit()
        var apiSer = apiClient.create(ApiService::class.java)

        apiSer.getFollowersUser(userName!!).enqueue(object : Callback<List<Users>> {
            override fun onResponse(call: Call<List<Users>>, response: Response<List<Users>>) {

                if(response.isSuccessful) {
                    if (response.body()?.size != 0) {

                        usersList.addAll(response.body()!!)
                        usersAdatoptor.notifyDataSetChanged()
                        progressDialog.dismiss()
                    } else {
                        Toast.makeText(context, "Sizni followerlaringiz yo'q", Toast.LENGTH_SHORT)
                            .show()
                        progressDialog.dismiss()
                    }
                }else{
                    Toast.makeText(context, "Response ${response.code()}", Toast.LENGTH_SHORT)
                        .show()
                    progressDialog.dismiss()
                }
            }

            override fun onFailure(call: Call<List<Users>>, t: Throwable) {
                progressDialog.dismiss()
                Toast.makeText(context,"Unable to connect to the Internet ", Toast.LENGTH_SHORT).show()
            }

        })
    }

}