package com.dilshodjon216.githubapp.framgemnt

import android.content.Context
import android.content.Context.CONNECTIVITY_SERVICE
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.dilshodjon216.githubapp.R
import com.dilshodjon216.githubapp.activity.DashbordActivity
import kotlinx.android.synthetic.main.fragment_splash.*
import kotlinx.android.synthetic.main.fragment_splash.view.*


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class SplashFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    lateinit var sharedpreferences: SharedPreferences
    private var MyPREFERENCES: String = "githubApp"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val inflate = inflater.inflate(R.layout.fragment_splash, container, false)

        sharedpreferences =
            context!!.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE)
        splash()

        inflate.refreshBtn.setOnClickListener {
            progress_circular.visibility = View.VISIBLE
            interntText1.visibility = View.INVISIBLE
            interntText2.visibility = View.INVISIBLE
            refreshBtn.visibility = View.INVISIBLE
            splash()
        }


        return inflate
    }
    @Suppress("DEPRECATION")
    private fun splash() {

        Handler().postDelayed({




            val systemService =
                activity?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            val networkInfo = systemService.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
            val networkInfo1 = systemService.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)

            if (networkInfo != null && networkInfo.isConnected || networkInfo1 != null && networkInfo1.isConnected) {
                progress_circular.visibility = View.INVISIBLE

                var user=sharedpreferences.getString("userLogin","")

                if(!user.equals("")) {

                    val intent = Intent(activity, DashbordActivity::class.java)
                    activity!!.startActivity(intent)
                    activity!!.finish()

                }else{
                    var loginFragment = SigInFragment()
                    fragmentManager?.beginTransaction()
                        ?.setCustomAnimations(R.anim.enter_right_to_left, R.anim.exit_right_to_left)
                        ?.add(R.id.content, loginFragment, "")
                        ?.commit()
                    fragmentManager?.popBackStack()
                }



            } else {
                progress_circular.visibility = View.INVISIBLE
                interntText1.visibility = View.VISIBLE
                interntText2.visibility = View.VISIBLE
                refreshBtn.visibility = View.VISIBLE
            }

        }, 2000)
    }

    private fun isNetworkAvailable(context: Context): Boolean {
        val cm = context.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        return cm.isActiveNetworkMetered


    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SplashFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            SplashFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}