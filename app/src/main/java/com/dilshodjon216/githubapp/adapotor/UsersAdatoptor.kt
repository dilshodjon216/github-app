package com.dilshodjon216.githubapp.adapotor

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dilshodjon216.githubapp.R
import com.dilshodjon216.githubapp.model.Users
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_dashbord.*
import kotlinx.android.synthetic.main.item_users.view.*

class UsersAdatoptor(var userList: ArrayList<Users>, var onclick: OnItemClick) :
    RecyclerView.Adapter<UsersAdatoptor.Vh>() {


    inner class Vh(view: View) : RecyclerView.ViewHolder(view) {
        fun onBand(users: Users) {
            itemView.nameUser.text = users.login
            itemView.bioUser.text=users.company
            itemView.userLoction.text=users.location

            Picasso.get().load(users.avatar_url).placeholder(R.drawable.ic_baseline_account_circle_24).into(
                itemView.usersImage
            )
         itemView.setOnClickListener {
             onclick.onClick(users)
         }

        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Vh {
        return Vh(
            LayoutInflater.from(parent.context).inflate(R.layout.item_users, parent, false)
        )
    }

    override fun onBindViewHolder(holder: Vh, position: Int) {
        holder.onBand(userList[position])

    }

    override fun getItemCount(): Int = userList.size

    interface OnItemClick {
        fun onClick(users: Users)
    }
}