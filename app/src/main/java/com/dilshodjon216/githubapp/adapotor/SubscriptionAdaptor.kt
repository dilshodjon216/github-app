package com.dilshodjon216.githubapp.adapotor

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.dilshodjon216.githubapp.R
import com.dilshodjon216.githubapp.model.Subscriptions
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_dashbord.*
import kotlinx.android.synthetic.main.item_subscriptions.view.*

class SubscriptionAdaptor (var subscriptions:ArrayList<Subscriptions>,var onClick:OnClickText,var context: Context):RecyclerView.Adapter<SubscriptionAdaptor.VH>(){

    inner class VH(view: View):RecyclerView.ViewHolder(view){
        fun onBand(subscriptions: Subscriptions,){
            itemView.projectName.text="Project name:    ${subscriptions.name.toUpperCase()}"
            itemView.createdDate.text="Created:             ${subscriptions.created_at.substring(0,10)}"
            itemView.projectDescription.text=subscriptions.description
            itemView.projectLanguage.text=subscriptions.language
            itemView.projectClone.text=subscriptions.clone_url
            itemView.projectUrl.text=subscriptions.html_url
            Picasso.get().load(subscriptions.owner.avatar_url).placeholder(R.drawable.ic_baseline_account_circle_24).into(
                itemView.imagePj
            )

            itemView.projectClone.setOnClickListener {
                onClick.onClick(subscriptions)
            }

            itemView.projectUrl.setOnClickListener {
                val url =subscriptions.html_url
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                context.startActivity(i)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
     return VH(LayoutInflater.from(parent.context).inflate(R.layout.item_subscriptions,parent,false))
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.onBand(subscriptions[position])
    }

    override fun getItemCount(): Int=subscriptions.size

    interface OnClickText{
        fun onClick(subscriptions: Subscriptions)
    }
}