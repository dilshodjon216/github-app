package com.dilshodjon216.githubapp.activity

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.dilshodjon216.githubapp.R
import com.dilshodjon216.githubapp.framgemnt.FollowersFragment
import com.dilshodjon216.githubapp.framgemnt.ProfileFragment
import com.dilshodjon216.githubapp.framgemnt.SubscriptionsFragment
import com.dilshodjon216.githubapp.framgemnt.UsersFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_dashbord.*

class DashbordActivity : AppCompatActivity() {

    lateinit var toolbar: Toolbar
    lateinit var sharedpreferences: SharedPreferences
    private var MyPREFERENCES: String = "githubApp"
    var coutNumber:Int=0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashbord)
        navigation.setOnNavigationItemSelectedListener(selectedListener)

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        sharedpreferences = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE)
        var userImage = sharedpreferences.getString("userImage", "")

        Picasso.get().load(userImage).placeholder(R.drawable.ic_baseline_account_circle_24).into(
            userImage1
        )

        exitbtn.setOnClickListener {

            val intent = Intent(this, MainActivity::class.java)
            this!!.startActivity(intent)
            this!!.finish()

            sharedpreferences.edit().clear().apply()
        }


        val homeFragment = UsersFragment()
        val fragmentTransaction1 = supportFragmentManager.beginTransaction()
        fragmentTransaction1.replace(R.id.contentII, homeFragment, "")
        nameChatTv.text = "Users"
        fragmentTransaction1.commit()
        coutNumber=1
    }

    private val selectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.nav_users -> {
                    coutNumber=1
                    val tableFragment = UsersFragment()
                    nameChatTv.text = "Users"
                    val fragmentTransaction2 = supportFragmentManager.beginTransaction()
                    fragmentTransaction2.replace(R.id.contentII, tableFragment, "")
                    fragmentTransaction2.commit()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.nav_followers -> {
                    coutNumber=2
                    nameChatTv.text = "Followers"
                    val historyFragment = FollowersFragment()
                    val fragmentTransaction3 = supportFragmentManager.beginTransaction()
                    fragmentTransaction3.replace(R.id.contentII, historyFragment, "")
                    fragmentTransaction3.commit()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.nav_subscriptions -> {
                    coutNumber=3
                    nameChatTv.text = "Subscriptions"
                    val settingsFragment = SubscriptionsFragment()
                    val fragmentTransaction4 = supportFragmentManager.beginTransaction()
                    fragmentTransaction4.replace(R.id.contentII, settingsFragment, "")
                    fragmentTransaction4.commit()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.nav_account -> {
                    coutNumber=4
                    nameChatTv.text = "Profile"
                    val settingsFragment = ProfileFragment()
                    val fragmentTransaction4 = supportFragmentManager.beginTransaction()
                    fragmentTransaction4.replace(R.id.contentII, settingsFragment, "")
                    fragmentTransaction4.commit()
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

    override fun onBackPressed() {

        when (coutNumber) {
            1 -> {
                super.onBackPressed()
            }
            2 -> {
                coutNumber=1
                val tableFragment = UsersFragment()
                nameChatTv.text = "Users"
                val fragmentTransaction2 = supportFragmentManager.beginTransaction()
                fragmentTransaction2.replace(R.id.contentII, tableFragment, "")
                fragmentTransaction2.commit()
            }
            3 -> {
                coutNumber=2
                nameChatTv.text = "Followers"
                val historyFragment = FollowersFragment()
                val fragmentTransaction3 = supportFragmentManager.beginTransaction()
                fragmentTransaction3.replace(R.id.contentII, historyFragment, "")
                fragmentTransaction3.commit()
            }
            4 -> {
                coutNumber=3
                nameChatTv.text = "Subscriptions"
                val settingsFragment = SubscriptionsFragment()
                val fragmentTransaction4 = supportFragmentManager.beginTransaction()
                fragmentTransaction4.replace(R.id.contentII, settingsFragment, "")
                fragmentTransaction4.commit()
            }
        }
    }
}