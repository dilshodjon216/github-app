package com.dilshodjon216.githubapp.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.dilshodjon216.githubapp.R
import com.dilshodjon216.githubapp.framgemnt.SplashFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var loginFragment = SplashFragment()
        supportFragmentManager.beginTransaction().add(R.id.content, loginFragment, "login")
            .commit()
    }
}