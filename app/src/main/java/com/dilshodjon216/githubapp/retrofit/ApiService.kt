package com.dilshodjon216.githubapp.retrofit

import com.dilshodjon216.githubapp.model.Subscriptions
import com.dilshodjon216.githubapp.model.Users
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {

     @GET("users")
     fun getUsers(): Call<List<Users>>

    @GET("users/{login}")
    fun getUser(@Path("login") login:String): Call<Users>
    @GET("users/{login}/followers")
    fun getFollowersUser(@Path("login")login: String):Call<List<Users>>
    @GET("users/{login}/subscriptions")
    fun getSubcriptions(@Path("login")login: String):Call<List<Subscriptions>>


}